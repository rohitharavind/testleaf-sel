package bi.framework.design;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.testcases.ReadExcel;


public class ProjectMethods extends SeleniumBase{
	public String dataSheet;	
@BeforeMethod
		public void login() {
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleUsername, "DemoSalesManager"); 
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, "crmsfa"); 
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			
	}

@DataProvider(name="fetchData")
public Object[][] readData(){
	

	
	return ReadExcel.readData(dataSheet);
}

}
