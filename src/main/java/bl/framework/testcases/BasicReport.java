
package bl.framework.testcases;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	@Test
	public void runReport(){
		html = new ExtentHtmlReporter("./report/extentReport.html");
		extent = new  ExtentReports();
		
		//To append the report for every run
		html.setAppendExisting(true);
		extent.attachReporter(html);
		
		//Testname and Description
		test = extent.createTest("TC002 CRM/SFA Link", "Click on CRM link");
		
		//Filter using UserName
		test.assignAuthor("Aravind");
		test.assignCategory("Regression");
		
		try {
			test.pass("User login is successful",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extent.flush();
		
		
		
		
	}
	
	
	

}
