package bl.framework.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;

public class TC002_CreateLead extends ProjectMethods {

	@Test(dataProvider="getData")
	@Parameters({"cLead"})
	public void CreateLead(String cName, String fName, String lName) throws InterruptedException {

				// Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmSfa);

		// Click Create Lead

		WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleCreateLead);

		// Enter CompanyName
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, cName);

		// Enter FirstName
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, fName);

		// Enter LastName
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastName, lName);

		// Enter Parental Id
		WebElement eleParental = locateElement("xpath", "//img[@src='/images/fieldlookup.gif']");
		click(eleParental);
		
		

		// switch to new window

		/*
		 * switchToWindow(1); System.out.println("Switched successfully"); try {
		 * Thread.sleep(2000); } catch (InterruptedException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } System.out.println(driver.getTitle());
		 * WebElement eleAccountId = locateElement("xpath", "//input[@name='id']");
		 * clearAndType(eleAccountId, "10008");
		 * 
		 * //Click Find Accounts WebElement eleFindButton = locateElement("xpath",
		 * "//button[text()='Find Accounts']"); click(eleFindButton); try {
		 * Thread.sleep(2000); } catch (InterruptedException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } WebElement eleValueId =
		 * locateElement("xpath", "//a[text()='10008']"); click(eleValueId);
		 */

		// Select value in Source dropdown
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Employee");

		// Enter Title Value
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		clearAndType(eleTitle, "Automation");

		// Enter Department
		WebElement eleDepartment = locateElement("id", "createLeadForm_departmentName");
		clearAndType(eleDepartment, "TestA");

		// Select Currency

		WebElement eleCurrency = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingValue(eleCurrency, "INR");

		// Select Industry
		WebElement eleIndustry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleIndustry, 1);

		// Enter ToName
		WebElement eleToName = locateElement("id", "createLeadForm_generalToName");
		clearAndType(eleToName, "Aravind.E");

		// Select Country from dropdown
		WebElement eleCountry = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingValue(eleCountry, "IND");

		Thread.sleep(2000);

		// Select State from dropdown
		WebElement eleState = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(eleState, "TAMILNADU");

		// Enter CreateLead Button
		WebElement eleSubmit = locateElement("class", "smallSubmit");
		click(eleSubmit);

		Thread.sleep(1500);

		// Get firstname
		WebElement eleFName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFName, "Aravind");

	}
	
	@Override
	public Object[][] readData() {
		// TODO Auto-generated method stub
		return super.readData();
	}
	
	/*@DataProvider(name="getData")
	public String[][] fetchData(){
		
		String[][] data = new String[2][3];
		data[0][0]="TestLeaf";
		data[0][1]="Aravind";
		data[0][2]="E";
		
		data[1][0]="TestLeaf";
		data[1][1]="Fname";
		data[1][2]="L";
		
		return data;*/
		
				
		
	

}
