package bl.framework.testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;

public class TC004_EditLead extends ProjectMethods {
	@Test
	public void EditLead() throws InterruptedException {
	

		// Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmSfa);
		
		//Click Leads
		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeads);
		
		//Click Find Leads
		WebElement eleFindLead = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLead);
		
		//Enter Lead ID
		WebElement eleLeadId = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleLeadId, "10313");
		
		//Click Find Leads
		WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindButton);
		Thread.sleep(1500);
		
		WebElement table = locateElement("class", "x-grid3-row-table");
		List<WebElement> row = table.findElements(By.tagName("tr"));
		WebElement firstRow = row.get(0);
		List<WebElement> column = firstRow.findElements(By.tagName("td"));
		String firstMatchingID = column.get(0).getText();
		
		WebElement elefirstMatchingID = locateElement("linkText", firstMatchingID);
		click(elefirstMatchingID);
		
		//Click Edit
		WebElement eleEditLead = locateElement("xpath", "//a[text()='Edit']");
		click(eleEditLead);
		Thread.sleep(1200);
		
		WebElement eleFname = locateElement("id", "updateLeadForm_firstName");
		clearAndType(eleFname, "Edited");
		
		//Click Update
		WebElement eleUpdate = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdate);
		Thread.sleep(1200);
		

		//Verify
		WebElement eleFName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFName, "Edited");
		
		 
		
		

	}

}
