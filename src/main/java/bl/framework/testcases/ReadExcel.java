package bl.framework.testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	static Object[][] data;

	public static Object[][] readData(String dataSheet)  {
		
		
		//Enter the workbook
		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./Data/"+dataSheet+".xlsx");
	
		
		//Enter the sheet
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		
		//Get the rowCount for length in loop
		int rowCount = sheet.getLastRowNum(); // returns the number of Last Row
		System.out.println(rowCount);
		
		//Get the cellCount
		int cellCount = sheet.getRow(0).getLastCellNum(); //Max cell value
		System.out.println(cellCount);
		data = new Object[rowCount][cellCount];
		
		
		for (int i = 1; i <= rowCount; i++) {
			//Entering row
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < cellCount; j++) {
				//Entering cell
				XSSFCell cell = row.getCell(j);
				data[i-1][j] = cell.getStringCellValue();
				
			}
			
		}
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;

	}

}
