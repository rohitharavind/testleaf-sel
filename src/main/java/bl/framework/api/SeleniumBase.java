package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import io.cucumber.datatable.dependency.difflib.myers.MyersDiff;

public class SeleniumBase implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	public Alert myAlert;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "linkText": return driver.findElementByLinkText(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		
		myAlert = driver.switchTo().alert();

	}

	@Override
	public void acceptAlert() {
		myAlert.accept();

	}

	@Override
	public void dismissAlert() {
		myAlert.dismiss();

	}
	

	@Override
	public String getAlertText() {
		String alertText = myAlert.getText();
		return alertText;
	}

	@Override
	public void typeAlert(String data) {
		myAlert.sendKeys(data);

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles =driver.getWindowHandles();
		List<String> ls = new ArrayList<String>(windowHandles);
		driver.switchTo().window(ls.get(index));

	}
	

	@Override
	public void switchToWindow(String title) {
		driver.switchTo().window(title);

	}

	@Override
	public void switchToFrame(int index) {
		
		driver.switchTo().frame(index);

	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		driver.switchTo().frame(ele);
		takeSnap();

	}

	@Override
	public void switchToFrame(String idOrName) {
		driver.switchTo().frame(idOrName);
		takeSnap();
		

	}

	@Override
	public void defaultContent() {
		
		driver.switchTo().defaultContent();
		takeSnap();

	}

	@Override
	public boolean verifyUrl(String url) {
		
		String currentUrl = driver.getCurrentUrl();
		if(currentUrl.equalsIgnoreCase(url)) {
			return true;
		}
		takeSnap();
		return false;
		
	}
	

	@Override
	public boolean verifyTitle(String title) {
		String currentTitle = driver.getTitle();
		if(currentTitle.equalsIgnoreCase(title)) {
			return true;
			
		}
		takeSnap();
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();

	}

	@Override
	public void quit() {
		driver.quit();

	}
	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		
	}

	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		
	}
	@Override
	public void append(WebElement ele, String data) {
		ele.sendKeys(data);
		takeSnap();
		
	}

	@Override
	public void clear(WebElement ele) {
		ele.clear();
		takeSnap();
		
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		ele.getText();
		takeSnap();
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		
		ele.getCssValue("backgroundColor");
		takeSnap();
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		ele.getText();
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		
		
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sel = new Select(ele);
		sel.selectByIndex(index);
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select sel = new Select(ele);
		sel.selectByValue(value);
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String inputText = ele.getText();
		
		if(inputText.equals(expectedText)) {
			System.out.println("Expected Text matches exactly");
			return true;
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String fullText = ele.getText();
		
		if(fullText.contains(expectedText)) {
			System.out.println("Partial Text matches exactly");
		}
		
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		String exactAttribute = ele.getAttribute(value);
		if(exactAttribute.equals(attribute)) {
			System.out.println("Attribute matches exactly");
		}
		takeSnap();
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String completeAttribute = ele.getAttribute(value);
		if(completeAttribute.contains(attribute)) {
			System.out.println("Attribute matches partially");
		}
		takeSnap();
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed()) {
			System.out.println("Element is visible");
		}
		takeSnap();
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if(!ele.isDisplayed()) {
			System.out.println("Element is not visible");
		}
		takeSnap();
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		if(ele.isEnabled()) {
			System.out.println("Element is enabled");
		}
		takeSnap();
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		if(ele.isSelected()) {
			System.out.println("Element is selected");
		}
		takeSnap();
		return false;
	}

}
