package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC005_MergeLead extends SeleniumBase {
	@Test
	public void MergeLead() throws InterruptedException {
		// Launch browser and URL
				startApp("chrome", "http://leaftaps.com/opentaps");
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

				// Identifying UserName Element
				WebElement eleUserName = locateElement("id", "username");

				// Giving UserName Input
				clearAndType(eleUserName, "DemoSalesManager");

				// Identifying PassWord Element
				WebElement elePassword = locateElement("id", "password");

				// Giving UserName Input
				clearAndType(elePassword, "crmsfa");

				// Identifying Login Button element
				WebElement eleLogin = locateElement("class", "decorativeSubmit");

				// Perform click action
				click(eleLogin);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Click CRM/SFA Link
				WebElement eleCrmSfa = locateElement("linkText", "CRM/SFA");
				click(eleCrmSfa);
				
				//Click Leads
				WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
				click(eleLeads);
				
				//Click Merge Leads
				WebElement eleMergeLead = locateElement("linkText", "Merge Leads");
				click(eleMergeLead);
				String defaultWindow = driver.getWindowHandle();
				System.out.println(defaultWindow);
				
				//Select from Lead ID
				WebElement eleFromId = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
				click(eleFromId);
				
				
				
				switchToWindow(1);
				/*Set<String> allwindowHandles = driver.getWindowHandles();
				List<String> ls = new ArrayList<String>();
				ls.addAll(allwindowHandles);
				driver.switchTo().window(ls.get(1));*/
				
				
				WebElement eleMfromId = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[1]");
				clearAndType(eleMfromId, "10234");
				
				
				
				
				//Click Find Lead
				WebElement elemFindLead = locateElement("xpath", "//button[text()='Find Leads']");
				click(elemFindLead);
				Thread.sleep(1200);
				
				
				
				
				
				
				WebElement table = locateElement("class", "x-grid3-row-table");
				List<WebElement> row = table.findElements(By.tagName("tr"));
				WebElement firstRow = row.get(0);
				List<WebElement> column = firstRow.findElements(By.tagName("td"));
				String firstMatchingID = column.get(0).getText();
				//driver.findElementByLinkText(firstMatchingID).click();;
				
				WebElement elefirstMatchingID = locateElement("linkText", firstMatchingID);
				click(elefirstMatchingID);
				
				
				//switch window
				switchToWindow(0);
				
				
				WebElement eleMtoId = locateElement("(//img[@src='/images/fieldlookup.gif'])[2]");
				click(eleMtoId);
				Thread.sleep(1200);
				
				switchToWindow(1);
				
				//switchToWindow("Merge Leads | opentaps CRM");
				
				String title1 = driver.getTitle();
				System.out.println(title1);
				WebElement eleMId = locateElement("xpath", "(//input[@class=' x-form-text x-form-field'])[1]");
				clearAndType(eleMId, "10106");
				
				
				
				//Click Find Lead
				WebElement elemFindLead2 = locateElement("xpath", "//button[text()='Find Leads']");
				click(elemFindLead2);
				Thread.sleep(1200);
				
				

	}

}
