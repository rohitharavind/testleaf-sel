package bl.framework.testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;

public class TC003_DuplicateLead extends ProjectMethods {
	
	@Test
	@Parameters({"link"})
	public void DuplicateLead(String link) throws InterruptedException {
		
		/*	
		// Launch browser and URL
				startApp("chrome", "http://leaftaps.com/opentaps");
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

				// Identifying UserName Element
				WebElement eleUserName = locateElement("id", "username");

				// Giving UserName Input
				clearAndType(eleUserName, "DemoSalesManager");

				// Identifying PassWord Element
				WebElement elePassword = locateElement("id", "password");

				// Giving UserName Input
				clearAndType(elePassword, "crmsfa");

				// Identifying Login Button element
				WebElement eleLogin = locateElement("class", "decorativeSubmit");

				// Perform click action
				click(eleLogin);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/

				// Click CRM/SFA Link
				WebElement eleCrmSfa = locateElement("linkText", link);
				click(eleCrmSfa);
				
				//Click Leads
				WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
				click(eleLeads);
				
				//Click Find Leads
				WebElement eleFindLead = locateElement("xpath", "//a[text()='Find Leads']");
				click(eleFindLead);
				
				//Enter Lead ID
				WebElement eleLeadId = locateElement("xpath", "//input[@name='id']");
				clearAndType(eleLeadId, "10852");
				
				//Click Find Leads
				WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Leads']");
				click(eleFindButton);
				Thread.sleep(1500);
				
				WebElement table = locateElement("class", "x-grid3-row-table");
				List<WebElement> row = table.findElements(By.tagName("tr"));
				WebElement firstRow = row.get(0);
				List<WebElement> column = firstRow.findElements(By.tagName("td"));
				String firstMatchingID = column.get(0).getText();
				
				WebElement elefirstMatchingID = locateElement("linkText", firstMatchingID);
				click(elefirstMatchingID);
				
				
				//Click on Duplicate Lead
				WebElement eleDuplicate = locateElement("linkText", "Duplicate Lead");
				click(eleDuplicate);
				
				//Click on Create Lead
				WebElement eleCreateLead = locateElement("xpath", "//input[@value='Create Lead']");
				click(eleCreateLead);
				Thread.sleep(1200); 
				
				//Verify
				WebElement eleFName = locateElement("id", "viewLead_firstName_sp");
				verifyExactText(eleFName, "Aravind");
				
				
				
				
				

	}

}
